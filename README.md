Run
---

```commandline
$ flask --app js_example --debug run
```

Open http://127.0.0.1:5000 in a browser.

Test
----

```commandline
$ pytest
```

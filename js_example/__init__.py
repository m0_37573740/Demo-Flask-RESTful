from flask import Flask

from js_example.api import api

app = Flask(__name__)
api.init_app(app)

from js_example import views

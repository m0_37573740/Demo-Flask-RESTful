from flask import request

from flask_restful import Resource, Api

api = Api()


class Add(Resource):
    def post(self):
        a = request.form.get("a", 0, type=float)
        b = request.form.get("b", 0, type=float)
        return {"result": a + b}


api.add_resource(Add, '/add')

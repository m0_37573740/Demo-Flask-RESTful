from flask import render_template

from js_example import app


@app.route("/", defaults={"js": "xhr"})
@app.route("/<any(xhr, jquery, fetch):js>")
def index(js):
    return render_template(f"{js}.html", js=js)
